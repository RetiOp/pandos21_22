#ifndef ASL_H_INCLUDED
#define ASL_H_INCLUDED

#include "pandos_types.h"

/* FUNZIONI GESTIONE DELLE ASL */

/* 14 */ int insertBlocked(int *semAdd,pcb_t *p);

/* 15 */ pcb_t *removeBlocked(int *semAdd);

/* 16 */ pcb_t *outBlocked(pcb_t *p);

/* 17 */ pcb_t *headBlocked(int *semAdd);

/* 18 */ void initASL();

/* Funzioni ausiliarie */
semd_t* getSemd(int *semAdd, struct list_head* semd_list);	// Dato un semAdd restituisce il semaforo corrispondente

void mvSemd(semd_t* s, struct list_head* semd_list); // Dato un semaforo lo sposta da semd_h a smdFree_h o viceversa

#endif
