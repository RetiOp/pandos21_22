#include "../h/pandos_types.h"
#include "../h/pandos_const.h"
#include "../h/pcb.h"
#include "../h/asl.h"
#include "../h/listx.h"
#include <umps3/umps/const.h>

// Definizione delle strutture dati utili alla gestione dei semafori
static semd_t semd_table[MAXPROC];
static struct list_head _semdFree_h=LIST_HEAD_INIT(_semdFree_h); // lista dei SEMD liberi o inutilizzati
static struct list_head* semdFree_h= &_semdFree_h;
static struct list_head _semd_h=LIST_HEAD_INIT(_semd_h); // lista dei semafori attivi
static struct list_head* semd_h= &_semd_h;

semd_t* getSemd(int *semAdd, struct list_head* semd_list) {
	if(!list_empty(semd_list)){
		struct list_head* tmp=list_next(semd_list);
		while (!list_is_last(tmp, semd_list)) {
			semd_t* s= container_of(tmp,semd_t,s_link);
			if(s->s_key==semAdd) return s;
			else tmp=list_next(tmp);
		}
		semd_t* s= container_of(tmp,semd_t,s_link);
		if(s->s_key==semAdd) return s;
	}
	return NULL;
}

void mvSemd(semd_t* s,struct list_head* semd_listMv){
	list_del(&s->s_link);
	list_add(&s->s_link, semd_listMv);
}


int insertBlocked(int *semAdd, pcb_t *p) {
	if(getSemd(semAdd, semd_h)==NULL){
		if(list_empty(semdFree_h)){
			return 1;
		}else{
			semd_t* tmp= container_of(list_next(semdFree_h),semd_t,s_link);
			tmp->s_procq.next=&tmp->s_procq;
			tmp->s_procq.prev=&tmp->s_procq;
			mvSemd(tmp,semd_h);
			tmp->s_key=semAdd;
			list_add_tail(&p->p_list,&tmp->s_procq);
			p->p_semAdd=semAdd;
			return 0;					
		}
	}else{
		list_add_tail(&p->p_list,&getSemd(semAdd, semd_h)->s_procq);
		p->p_semAdd=semAdd;
		return 0;
	}
}


pcb_t *removeBlocked(int *semAdd) {
	if(getSemd(semAdd, semd_h)==NULL){
		return NULL;
	}else{
		semd_t*s=getSemd(semAdd, semd_h);
		pcb_t* p=container_of(list_next(&s->s_procq),pcb_t,p_list);
		list_del(&p->p_list);
		p->p_semAdd=NULL;
		if(list_empty(&s->s_procq)){
			mvSemd(s, semdFree_h);			
		}
		return p;		
	}
	return NULL;
}




pcb_t *outBlocked(pcb_t *p) {
	semd_t* s=getSemd(p->p_semAdd, semd_h);	
	if(s!=NULL){
		if(!list_empty(&s->s_procq)){
			struct list_head* tmp=list_next(&s->s_procq);
			while (!list_is_last(tmp,&s->s_procq)) {
				if(tmp==&p->p_list){
					list_del(tmp);
					if(list_empty(&s->s_procq)){
						mvSemd(s, semdFree_h);
					}
					return container_of(tmp, pcb_t, p_list);
				}else tmp=list_next(tmp);				
			}
			if(tmp==&p->p_list){
				list_del(tmp);
				if(list_empty(&s->s_procq)){
					mvSemd(s, semdFree_h);
				}
				return container_of(tmp, pcb_t, p_list);
			}else return NULL;
		}
	}
}

pcb_t *headBlocked(int *semAdd) {
	semd_t* s=getSemd(semAdd, semd_h);
	if(s!=NULL){
		if(!list_empty(&s->s_procq)){
			return container_of(list_next(&s->s_procq),pcb_t,p_list);
		}
	}
	return NULL;
}

void initASL() {
	for(int k = 0;  k < MAXPROC; k++){
    	list_add(&semd_table[k].s_link, semdFree_h);    
    } 
}











